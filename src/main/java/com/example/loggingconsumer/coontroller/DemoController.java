package com.example.loggingconsumer.coontroller;

import com.example.loggingconsumer.component.MessageProvider;
import com.example.loggingconsumer.dto.Person;
import javax.annotation.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

  @Resource
  private DirectChannel directChannel;

  @Resource
  private MessageProvider messageProvider;

  @PostMapping("sample1")
  public ResponseEntity<Void> sendPerson() {
    String name = "vincent";
    Person p = new Person();
    p.setName(name);

    directChannel.send(new GenericMessage<>(p));
    return ResponseEntity.noContent().build();
  }

  @PostMapping("sample2")
  public ResponseEntity<Void> sendMessage() {
    messageProvider.send();
    return ResponseEntity.noContent().build();
  }

}

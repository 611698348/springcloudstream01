package com.example.loggingconsumer.dto;

import lombok.Data;

@Data
public class Person {
  private String name;
}
